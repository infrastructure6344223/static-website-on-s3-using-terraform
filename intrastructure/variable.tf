variable "AWS_REGION" {
    type = string
    description = "The name of the bucket"
}

variable "S3_WEBSITE_BUCKET_NAME" {
    type = string
    description = "The name of the bucket"
}

variable "S3_REMOTE_STATE_BUCKET_NAME" {
    type = string
    description = "The name of the bucket"
}

variable "WEBSITE_COMMON_TAGS" {
    type = map(string)
    description = "Common tags you can applied to all components"
}